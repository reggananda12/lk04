import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try  {
                System.out.println("Masukkan Pembilang : ");
            int pembilang = scanner.nextInt();

                System.out.println("Masukkan Penyebut : ");
            int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil : " + hasil);

                validInput = true;
            } 
            catch (InputMismatchException e) {
                System.out.println("Input harus bilangan integer. Silakan coba lagi.");
                scanner.nextLine();
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        if (penyebut == 0) {
            throw new ArithmeticException("Penyebut tidak boleh bernilai 0");
        }
        return pembilang / penyebut;
    }
}